"""Default values.

"""

renormalization = 1             # Renormalization paramter (0 <= alpha <= 1).

num_eigenpairs = 11            # Number of eigenvalue/eigenvectors to obtain.

use_cuda = True                 # Use GPU-accelerated code.
