"""Diffusion maps module.

This module implements the diffusion maps method for dimensionality
reduction, as introduced in:

Coifman, R. R., & Lafon, S. (2006). Diffusion maps. Applied and Computational
Harmonic Analysis, 21(1), 5–30. DOI:10.1016/j.acha.2006.04.006

"""

__all__ = ['DiffusionMaps', 'downsample']


import logging
from typing import Optional, Dict, Tuple

import numpy as np
import scipy
import scipy.sparse
from scipy.spatial import cKDTree
import scipy.spatial as spsp

from . import default
from . import utils
from . import clock


def downsample(data: np.array, num_samples: int) -> np.array:
    """Randomly sample a subset of a data set while preserving order.

    The sampling is done without replacement.

    Parameters
    ----------
    data : np.array
        Array whose 0-th axis indexes the data points.
    num_samples : int
        Number of items to randomly (uniformly) sample from the data.  This
        is typically less than the total number of elements in the data set.

    Returns
    -------
    sampled_data : np.array
       A total of `num_samples` uniformly randomly sampled data points from
       `data`.

    """
    assert num_samples <= data.shape[0]
    indices = sorted(np.random.choice(range(data.shape[0]), num_samples,
                                      replace=False))
    return (data[indices, :],indices)


class DiffusionMaps:
    """Diffusion maps.

    Attributes
    ----------
    epsilon : float
        Bandwidth for kernel.
    _cut_off : float
        Cut off for the computation of pairwise distances between points.
    _kdtree : cKDTree
        KD-tree for accelerating pairwise distance computation.
    kernel_matrix : scipy.sparse.spmatrix
        (Possibly stochastic) matrix obtained by evaluating a Gaussian kernel
        on the data points.
    renormalization : float or None
        Renormalization exponent (alpha in the diffusion maps literature).
    eigenvectors : np.array
        Right eigenvectors of `kernel_matrix`.
    eigenvalues : np.array
        Eigenvalues of `kernel_matrix`.

    """
    def __init__(self, points: np.array, epsilon: float,
                 cut_off: Optional[float] = None,
                 num_eigenpairs: Optional[int] = default.num_eigenpairs,
                 normalize_kernel: Optional[bool] = True,
                 renormalization: Optional[float] = default.renormalization,
                 kdtree_options: Optional[Dict] = None,
                 use_cuda: Optional[bool] = default.use_cuda,
                 metric: Optional[str] = 'minkowski',
                 pnorm: Optional[int] = 2,
                 inputs : Optional[np.array] = 0,
                 epsilon_inputs: Optional[float] = 1,
                 covi: Optional[np.array] = np.identity(1)) \
            -> None:
        """Compute diffusion maps.

        This function computes the eigendecomposition of the transition
        matrix associated to a random walk on the data using a bandwidth
        (time) equal to epsilon.

        Parameters
        ----------
        points : np.array
            Data set to analyze. Its 0-th axis must index each data point.
        epsilon : float
            Bandwidth to use for the kernel.
        cut_off : float, optional
            Cut-off for the distance matrix computation. It should be at
            least equal to `epsilon`.
        num_eigenpairs : int, optional
            Number of eigenpairs to compute. Default is
            `default.num_eigenpairs`.
        normalize_kernel : bool, optional
            Whether to convert the kernel into a stochastic matrix or
            not. Default is `True`.
        renormalization : float, optional
            Renormalization exponent to use if `normalize_kernel` is
            True. This is the parameter $\alpha$ in the diffusion maps
            literature. It must take a value between zero and one.
        kdtree_options : dict, optional
            A dictionary containing parameters to pass to the underlying
            cKDTree object.
        use_cuda : bool, optional
            Determine whether to use CUDA-enabled eigenvalue solver or not.

        """
        self.epsilon = epsilon

        self._cut_off = (cut_off if cut_off is not None
                         else self.__get_cut_off(self.epsilon))
        self.points = points
        self.covi = covi
        self._kdtree = self.compute_kdtree(points, kdtree_options)
        if metric=='informed':
            self._kdtreeinput = self.compute_kdtree(inputs, kdtree_options)
        else:
            self.kdtreeinput = None


        self.metric = metric
        self.pnorm = pnorm

        distance_matrix = utils.coo_tocsr(self.compute_distance_matrix(kdtree_options))
        if metric=='informed':
            input_distance_matrix \
                = self._kdtreeinput.sparse_distance_matrix(self._kdtreeinput,
                                                      self._cut_off,
                                                      p = self.pnorm)
            input_distance_matrix = utils.coo_tocsr(input_distance_matrix)

        kernel_matrix = self.compute_kernel_matrix(distance_matrix,self.epsilon)
        if metric=='informed':
            kernel_matrix = kernel_matrix + self.compute_kernel_matrix(input_distance_matrix,epsilon_inputs)

        if normalize_kernel is True:
            kernel_matrix = self.normalize_kernel_matrix(kernel_matrix,
                                                         renormalization)
        self.kernel_matrix = kernel_matrix
        self.renormalization = renormalization if normalize_kernel else None

        ew, ev = self.solve_eigenproblem(self.kernel_matrix, num_eigenpairs,
                                         use_cuda)
        self.eigenvalues = ew
        self.eigenvectors = ev

    @staticmethod
    def __get_cut_off(epsilon: float) -> float:
        """Return a reasonable cut off value.

        """
        return 2.0 * epsilon  # XXX Validate this.

    @staticmethod
    @clock.log
    def compute_kdtree(points: np.array, kdtree_options: Optional[Dict]) \
            -> None:
        """Compute kd-tree from points.

        """
        if kdtree_options is None:
            kdtree_options = dict()
        return cKDTree(points, **kdtree_options)

    @clock.log
    def compute_distance_matrix(self,kdtree_options) -> scipy.sparse.coo_matrix:
        """Compute sparse distance matrix in COO format.

        """
        if self.metric == 'minkowski':
            distance_matrix \
                = self._kdtree.sparse_distance_matrix(self._kdtree,
                                                      self._cut_off,
                                                      p = self.pnorm)

            logging.debug('Distance matrix has {} nonzero entries ({:.4f}% dense).'
                          .format(distance_matrix.nnz, distance_matrix.nnz
                               / np.prod(distance_matrix.shape)))

        elif self.metric == 'informed':
            distance_matrix \
                = self._kdtree.sparse_distance_matrix(self._kdtree,
                                                      self._cut_off,
                                                      p = self.pnorm)
        elif self.metric == 'mahalanobis':
            dense_distance_matrix = np.zeros((len(self.points),len(self.points)))
            for i in range(len(self.points)):
                for j in range(i,len(self.points)):
                    covi_ij = self.covi[i]+self.covi[j]
                    dist = spsp.distance.mahalanobis(self.points[i],self.points[j],covi_ij)
                    if dist < self._cut_off:
                        dense_distance_matrix[i,j] = dist
                        dense_distance_matrix[j,i] = dist
            #import pdb
            #pdb.set_trace()
            distance_matrix = scipy.sparse.coo_matrix(dense_distance_matrix)


            logging.debug('Distance matrix has {} nonzero entries ({:.4f}% dense).'
                          .format(distance_matrix.nnz, distance_matrix.nnz
                               / np.prod(distance_matrix.shape)))
        else:
            raise Exception("Unsupported distance metric: choose minkowski or mahalanobis")

        return distance_matrix

    @clock.log
    def compute_kernel_matrix(self, distance_matrix: scipy.sparse.spmatrix,eps : float) \
            -> scipy.sparse.spmatrix:
        """Compute kernel matrix.

        Returns the (unnormalized) Gaussian kernel matrix corresponding to
        the data set and choice of bandwidth `epsilon`.

        Parameters
        ----------
        distance_matrix : scipy.sparse.spmatrix
            A sparse matrix whose entries are the distances between data
            points.

        Returns
        -------
        kernel_matrix : scipy.sparse.spmatrix
            A similarity matrix (unnormalized kernel matrix) obtained by
            applying `kernel_function` to the entries in `distance_matrix`.

        """
        data = distance_matrix.data
        transformed_data = self.kernel_function(data,eps)
        kernel_matrix = distance_matrix._with_data(transformed_data, copy=True)
        return kernel_matrix

    @clock.log
    def normalize_kernel_matrix(self, matrix: scipy.sparse.csr_matrix,
                                alpha: Optional[float] = 1) \
            -> scipy.sparse.csr_matrix:
        """Compute normalized random walk Laplacian from similarity matrix.

        Parameters
        ----------
        matrix : scipy.sparse.csr_matrix
            A similarity matrix obtained by evaluating a kernel function on a
            distance matrix.
        alpha : float, optional
            Renormalization parameter. The value of `alpha` must lie in the
            closed unit interval.

        Returns
        -------
        matrix : scipy.sparse.csr_matrix
            A (suitably normalized) row-stochastic random walk Laplacian.

        """
        assert 0 <= alpha <= 1, 'Invalid normalization exponent.'

        if alpha > 0:
            shape = matrix.shape

            row_sums = np.asarray(matrix.sum(axis=1)).squeeze()

            inv_diag = 1.0 / row_sums**alpha
            inv_diag[np.isnan(inv_diag)] = 0.0
            Dinv = scipy.sparse.spdiags(inv_diag, 0, shape[0], shape[1])

            Wtilde = Dinv * matrix * Dinv

            return self.make_stochastic_matrix(Wtilde)
        else:
            return self.make_stochastic_matrix(matrix)

    @staticmethod
    @clock.log
    def make_stochastic_matrix(matrix: scipy.sparse.csr_matrix) \
            -> scipy.sparse.csr_matrix:
        """Convert a sparse non-negative matrix into a row-stochastic matrix.

        Carries out the normalization (in the 1-norm) of each row of a
        non-negative matrix inplace.  The matrix should be in Compressed
        Sparse Row format. Note that this method overwrites the input matrix.

        Parameters
        ----------
        matrix : scipy.sparse.csr_matrix
            A matrix with non-negative entries to be converted.

        Returns
        -------
        matrix : scipy.sparse.csr_matrix
            The same matrix passed as input but normalized.

        """
        data = matrix.data
        indptr = matrix.indptr
        for i in range(matrix.shape[0]):
            a, b = indptr[i:i+2]
            norm1 = np.sum(data[a:b])
            data[a:b] /= norm1

        return matrix

    @clock.log
    def kernel_function(self, distances: np.array,eps:float) -> np.array:
        """Evaluate kernel function.

        """
        return np.exp(-np.square(distances) / (2.0 * eps))

    @clock.log
    def solve_eigenproblem(self, kernel_matrix: scipy.sparse.csr_matrix,
                           num_eigenpairs: int, use_cuda: bool) \
            -> Tuple[np.array, np.array]:
        """Solve eigenvalue problem using CPU or GPU solver.

        """
        if use_cuda is True:
            from .gpu_eigensolver import eigensolver
        else:
            from .cpu_eigensolver import eigensolver

        return eigensolver(kernel_matrix, num_eigenpairs)

    def residuals(self, eps_med_scale):
        """Computes the local linear regression error
        for each of the DMAPS eigenvectors as a function
        of the previous eigenvectors.
        The linear regression kernel is a Gaussian
        with width median(distances)/eps_med_scale.
        In the returned residuals, res
        Args:
            eps_med_scale (float): the scale to use in the
            local linear regression kernel, typically around eps/3
            .. note:: V[:,0] is assumed to be the trivial constant eigenvector
        Returns:
            residuals (array): the residuals of each of the fitted functions.
            residuals[i] is close to 1 if V[:,i]
            parameterizes a new direction in the data.
            ..note:: residuals[0] should be ignored, and residuals[1] is always 1
        """
        # real part of dmaps eigenvectors
        V = np.real(self.eigenvectors.T)
        __neigvects = V.shape[1]
        __residuals = np.zeros(__neigvects)
        __residuals[1] = 1.0

        for i in range(2,__neigvects):
            __residuals[i] = self._local_linear_regression(V[:,i], V[:, 1:i], eps_med_scale)

        return __residuals
    #####################################################
    def _local_linear_regression(self,y, X, eps_med_scale):
        """There is some math here"""

        n = X.shape[0]
        nvects = X.shape[1]

        #K = np.empty((n, n))
        #for i in range(n):
        #    K[i,i] = 0.0
        #    for j in range(i+1, n):
        #        K[i,j] = np.linalg.norm(X[i] - X[j])
        #        K[j,i] = K[i,j]
        K = spsp.distance.squareform(spsp.distance.pdist(X,'euclidean'))

        eps = np.median(K)/eps_med_scale
        W = np.exp(-np.power(K/eps, 2))

        L = np.zeros((n,n));
        for i in range(n):
            Xx = np.hstack((np.ones((n,1)), X - np.ones((n, nvects))*X[i]))
            Xx2 = Xx.T*W[i]
            A = np.linalg.lstsq(np.dot(Xx2, Xx), Xx2)[0]
            L[i] = A[0]

        fx = np.dot(L, y)
        diagL = np.diagonal(L)
        #import pdb
        #pdb.set_trace()
        numerators = y-fx
        denominators = 1-diagL

        result = np.zeros(n)
        for i in range(len(numerators)):
            if numerators[i] ==0:
                result[i] = 0
            elif denominators[i] ==0:
                result[i] = np.std(y,ddof=1)
            else:
                result[i] = numerators[i]/denominators[i]
        resid = np.sqrt(np.average(result**2))/np.std(y, ddof=1)
        #if resid>1:
            #import pdb
            #pdb.set_trace()
        return resid
