__all__ = ['version']


class version:
    """Current version."""
    v_short = '20170616.10'
    v_long = '20170616.10 (2017-Jun-16)'
    v_gnu = 'diffusion_maps 20170616.10 (2017-Jun-16)'
